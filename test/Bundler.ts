"use strict";
import * as File from "vinyl";
import CreateBundle from "./scenario/CreateBundle";

describe("Bundler", () => {
    it("should transform vinyl files into a bundle", () => {
        const cwd = "/some/test";
        const expectedModules: Array<{ bundle: string, id: string, src: string, deps: string }> = [
            {
                bundle: "Common.js",
                deps: `"../test4":"test4.js","./test2":"path/test2.js",` +
                    `"/some/test/path/test2":"path/test2.js","path/test2":"path/test2.js"`,
                id: "path/test1.js",
                src: `const x = require('path/test2');
                      const z = require('../test4');
                      const a = require('./test2');
                      const b = require('/some/test/path/test2');

                      const y = { test: x };`,
            },
            { bundle: "Common.js", id: "path/test2.js", src: "{ test: 2 }", deps: "" },
            { bundle: "Common.js", id: "test4.js", src: "{ test: 4 }", deps: "" },
        ];

        const inputFiles: File[] = [];
        expectedModules.forEach(file => {
            inputFiles.push(new File({
                contents: new Buffer(file.src),
                cwd,
                path: cwd + "/" + file.id }));
        });

        return CreateBundle
            .given(inputFiles, [{ name: "Common", include: ["path/test1"] }])
            .expectAllSpecifiedBundles(expectedModules)
            .expectRelativePathsWithoutExtension(expectedModules)
            .expectSourceForResolvedPath(expectedModules)
            .done();
    });
    it("should expose a bundle using the specified name", () => {
        const cwd = "/some/test";
        const expectedModules: Array<
            { bundle: string | undefined, id: string, path: string, src: string, deps: string }> = [
            {
                bundle: "Common.js",
                deps: "",
                id: "MyModule",
                path: "path/test1.js",
                src: "{ test: 1 }",
            },
            {
                bundle: undefined, // It is excluded so not in any bundle.
                deps: "",
                id: "MySecondModule",
                path: "path/test3.js",
                src: "{ test: 3 }",
            },
            {
                bundle: "Common.js",
                deps: `"./test1":"MyModule","./test3":"MySecondModule"`,
                id: "path/test2.js",
                path: "path/test2.js",
                src: "const x = require('./test1'); const y = require('./test3');",
            },
        ];

        const inputFiles: File[] = [];
        expectedModules.forEach(file => {
            inputFiles.push(new File({
                contents: new Buffer(file.src),
                cwd,
                path: cwd + "/" + file.path }));
        });

        return CreateBundle
            .given(inputFiles, [
                {
                    exclude: [ "MySecondModule" ],
                    include: [
                        { id: "path/test1", name: "MyModule" },
                        { id: "path/test3", name: "MySecondModule" },
                        "path/test2",
                    ],
                    name: "Common",
                }])
            .expectAllSpecifiedBundles(expectedModules)
            .expectRelativePathsWithoutExtension(expectedModules)
            .expectSourceForResolvedPath(expectedModules)
            .done();
    });
    it("should exclude the specified modules from the bundle", () => {
        const cwd = "/some/test";
        const expectedModules: Array<{ bundle: string | undefined, id: string, src: string, deps: string }> = [
            {
                bundle: "Page.js",
                deps: `"../test4":"test4.js","path/test2":"path/test2.js"`,
                id: "path/test1.js",
                src: `const x = require('path/test2');
                      const z = require('../test4');

                      const y = { test: x };`,
            },
            {
                bundle: "Common.js",
                deps: `"../test5":"test5.js"`,
                id: "path/test2.js",
                src: "const z = require('../test5'); { test: 2 }",
            },
            { bundle: "Common.js", id: "test5.js", src: "{ test: 5 }", deps: "" },
            { bundle: "Common.js", id: "test6.js", src: "{ test: 6 }", deps: "" },
            // test4 is not in any bundle as specfied below.
            { bundle: undefined, id: "test4.js", src: "{ test: 4 }", deps: "" },
        ];

        const inputFiles: File[] = [];
        expectedModules.forEach(file => {
            inputFiles.push(new File({
                contents: new Buffer(file.src),
                cwd,
                path: cwd + "/" + file.id }));
        });

        return CreateBundle
            .given(
                inputFiles,
                [
                    { name: "Common", include: ["path/test2", "test6"] },
                    { name: "Page", include: ["path/test1"], exclude: ["Common", "test4"]},
                ])
            .expectAllSpecifiedBundles(expectedModules)
            .expectRelativePathsWithoutExtension(expectedModules)
            .expectSourceForResolvedPath(expectedModules)
            .done();
    });
    it("should bundle external node modules", () => {
        const cwd = process.cwd() + "/path";
        const expectedModules: Array<{ bundle: string, id: string, src: string, deps: string }> = [
            {
                bundle: "Common.js",
                deps: `"subarg":"node_modules/subarg/index.js"`,
                id: "path/test1.js",
                src: `const x = require("subarg"); const y = { test: x };`,
            },
            {
                bundle: "Common.js",
                deps: `"minimist":"node_modules/minimist/index.js"`,
                id: "node_modules/subarg/index.js",
                src: "",
            },
            { bundle: "Common.js", id: "node_modules/minimist/index.js", src: "", deps: "" },
        ];

        const inputFiles: File[] = [];
        inputFiles.push(new File({
            contents: new Buffer(expectedModules[0].src),
            cwd,
            path: cwd + "/" + expectedModules[0].id }));

        return CreateBundle
            .given(inputFiles, [{ name: "Common", include: ["path/test1"] }])
            .expectAllSpecifiedBundles(expectedModules)
            .expectRelativePathsWithoutExtension(expectedModules)
            .done();
    });
});
