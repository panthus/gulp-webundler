"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util = require("gulp-util");
const through = require("through2");
const CreateBundle_1 = require("./CreateBundle");
/**
 * A stream transforming the input consisting of vinyl files into an output
 * stream consisting of bundles.
 */
class Bundler {
    /**
     * Creates the stream which transforms the vinyl files into a buffer and
     * outputs bundles.
     * @param bundleSpecification The specifications of the bundles which should
     * be generated from the inputted vinyl files.
     * @returns Returns the stream.
     */
    static bundle(bundleSpecification) {
        const bundle = new Bundler(bundleSpecification);
        return bundle.createStream();
    }
    /**
     * Initializes a new instance of the {@link Bundler} class.
     * @param bundleSpecification The specifications of the bundles which should
     * be generated from the inputted vinyl files.
     */
    constructor(bundleSpecification) {
        // Create bundle name to included modules map.
        const bundleMap = bundleSpecification.reduce((previous, current) => {
            previous[current.name] = current.include
                .map(x => x instanceof String || typeof x === "string" ? x : x.name);
            return previous;
        }, {});
        // Replace all bundle names in the exclude for the actual root module ids.
        this.bundleSpecification = bundleSpecification.map(x => {
            return {
                exclude: x.exclude
                    ? x.exclude.reduce((previous, current) => bundleMap[current]
                        ? previous.concat(bundleMap[current])
                        : previous.concat(current), [])
                    : x.exclude,
                include: x.include,
                name: x.name,
            };
        });
    }
    /**
     * Creates the stream which transforms the vinyl files into a cache and
     * outputs bundles.
     * @returns Returns the stream.
     */
    createStream() {
        this.cache = {};
        this.stream = through.obj((data, enc, cb) => { this.transform(data, enc, cb); }, cb => { this.flush(cb); });
        return this.stream;
    }
    /**
     * Create the cache from the input.
     */
    transform(data, enc, cb) {
        // Normalize to Unix path style so output is consistent between Windows and Unix systems.
        this.baseDir = data.base.replace(/\\/g, "/");
        this.cache[data.relative.replace(/\\/g, "/")] = data.contents;
        cb();
    }
    /**
     * Output the bundles when they are created.
     */
    flush(cb) {
        const createBundle = new CreateBundle_1.default(this.cache, this.baseDir);
        const pendingBundles = this.bundleSpecification.map(x => {
            return createBundle.create(x).then(bundle => {
                this.stream.push(bundle);
            });
        });
        Promise.all(pendingBundles)
            .then(() => { cb(); })
            .catch(error => {
            cb(new util.PluginError("gulp-webundler", error));
        });
    }
}
exports.default = Bundler.bundle;
