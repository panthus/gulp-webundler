"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const pack = require("browser-pack");
const deps = require("module-deps");
const path = require("path");
const nodeResolve = require("resolve");
const source = require("vinyl-source-stream");
/**
 * A class which can create the actual bundle given a bundle specification.
 * It uses module-deps and browser-pack to do the heavy lifting.
 */
class CreateBundle {
    /**
     * Initializes a new instance of the {@link CreateBundle} class. It is recommended
     * to create this class once and then create many bundles using {@link create}.
     * @param cache The cache containing the files out of which bundles are created. File
     * paths in this cache must be in Unix (/) style.
     * Note that files which are not part of this cache can still be used if the NodeJs
     * resolve (require) algorithm can find them.
     * @param baseDir The directory to which the local modules in the bundle are relative.
     * It must be in Unix (/) style.
     */
    constructor(cache, baseDir) {
        this.cache = cache || {};
        this.baseDir = baseDir || process.cwd();
    }
    /**
     * Creates the specified bundle.
     * @param bundleSpecification The specification of the bundle to be created.
     * @returns Retruns a promise consisting of a vinyl file which represents the
     * created bundle.
     */
    create(bundleSpecification) {
        return __awaiter(this, void 0, void 0, function* () {
            let modules = yield this.resolveDependencies(bundleSpecification.include);
            modules = yield this.excludeModules(modules, bundleSpecification.exclude);
            return yield this.pack(bundleSpecification.name, modules);
        });
    }
    /**
     * Resolve the dependencies for the given root module ids.
     * @param moduleSpecs The modules ids or module specifications for which the dependencies should be
     * resolved.
     * @returns Returns a promise which resolves into a flattened list of
     * modules. This includes the root modules and their children recursively
     * and in the correct order so a browser can parse it.
     */
    resolveDependencies(moduleSpecs) {
        return new Promise((resolve, reject) => {
            const resolvedObjectModuleSpecs = [];
            const tasks = moduleSpecs
                .reduce((previous, current) => {
                if (!(current instanceof String || typeof current === "string")) {
                    return previous.concat(this.resolveDependentModuleId(current.id, this.baseDir)
                        .then(resolvedModuleId => {
                        resolvedObjectModuleSpecs.push(Object.assign({}, current, { id: resolvedModuleId }));
                    }));
                }
                return previous;
            }, []);
            Promise.all(tasks)
                .then(() => {
                const modules = [];
                /**
                 * Create an instance of the module-deps plugin. Pass the buffer so
                 * we do not need to read anything from disk. Also override resolve
                 * because the default resolve checks if the ids exist on disk
                 * (which is not the case if they only exist in the cache, e.g. we
                 * piped this directly to compiler output).
                 */
                const depResolver = deps({
                    basedir: this.baseDir,
                    fileCache: this.cache,
                    resolve: (id, parent, cb) => {
                        this.resolveDependentModuleId(id, parent.id)
                            .then(moduleId => { cb(undefined, moduleId); })
                            .catch(error => { cb(error, undefined); });
                    },
                });
                moduleSpecs.forEach(x => {
                    depResolver.write((x instanceof String || typeof x === "string"
                        ? { file: x }
                        : { file: x.id, id: x.name }));
                });
                depResolver.on("data", module => {
                    // If a custom module name is specified then make sure all deps also use it.
                    for (const dep in module.deps) {
                        if (module.deps.hasOwnProperty(dep)) {
                            const moduleSpec = resolvedObjectModuleSpecs.find(x => x.id === module.deps[dep]);
                            if (moduleSpec) {
                                module.deps[dep] = moduleSpec.name;
                            }
                        }
                    }
                    modules.push(module);
                });
                depResolver.on("end", () => { resolve(modules); });
                depResolver.on("error", (error) => { reject(error); });
                depResolver.end();
            })
                .catch((error) => { reject(error); });
        });
    }
    /**
     * Exclude the given module ids from the specified modules list. Dependencies of
     * the specified modules are also excluded.
     * @param modules The modules from which modules are excluded.
     * @param excludeModuleIds The module ids to exclude. Can be null or undefined.
     * @returns Returns a promise containing the modules which remain after excluding.
     */
    excludeModules(modules, excludeModuleIds) {
        return new Promise((resolve, reject) => {
            if (!excludeModuleIds) {
                resolve(modules);
            }
            else {
                // Resolve the specified module names to exclude.
                const resolvedExcludeModuleIds = [];
                const tasks = excludeModuleIds.map(moduleId => {
                    if (modules.some(x => x.id === moduleId)) {
                        resolvedExcludeModuleIds.push(moduleId);
                        return Promise.resolve();
                    }
                    return this.resolveDependentModuleId(moduleId, this.baseDir)
                        .then(resolvedModuleId => { resolvedExcludeModuleIds.push(resolvedModuleId); });
                });
                Promise.all(tasks)
                    .then(() => {
                    // Continue looping until there are no more module ids to exclude.
                    while (resolvedExcludeModuleIds.length > 0) {
                        // Dequeue the first module id to exclude.
                        const moduleId = resolvedExcludeModuleIds.splice(0, 1)[0];
                        let index = -1;
                        const module = modules.find((x, i) => {
                            index = i;
                            return x.id === moduleId;
                        });
                        if (module) {
                            // Get the dependencies of the module we exclude so we can also exclude them.
                            const dependencies = [];
                            for (const dep in module.deps) {
                                if (module.deps.hasOwnProperty(dep)) {
                                    dependencies.push(module.deps[dep]);
                                }
                            }
                            Array.prototype.push.apply(resolvedExcludeModuleIds, dependencies);
                            // Remove excluded module.
                            modules.splice(index, 1);
                        }
                    }
                    resolve(modules);
                })
                    .catch((error) => { reject(error); });
            }
        });
    }
    /**
     * Pack the specified modules into a bundle with the givne name.
     * @param bundleName The name of the bundle without extension.
     * @param modules The modules which make up the bundle.
     * @returns Returns a promise which resolves to a vinyl file containing the
     * bundle.
     */
    pack(bundleName, modules) {
        return new Promise((resolve, reject) => {
            /**
             * Enable raw so we can write {@link ModuleDeps.IModule} objects to
             * the stream. Enable hasExports so require is available outside the
             * bundle.
             */
            const packer = pack({ hasExports: true, raw: true });
            modules.forEach(module => { packer.write(module); });
            packer.end();
            packer.pipe(source(bundleName + ".js"))
                .on("data", (bundle) => { resolve(bundle); })
                .on("error", (error) => { reject(error); });
        });
    }
    /**
     * Resolve the given dependent module id which is relative to its parent to
     * an id relative to the project root. For example a parent like '/some/parent'
     * results in the following outputs:
     * ./dependent => some/dependent.js (basedir is stripped and same dir)
     * ../dependent => dependent.js (basedir is stripped and one dir up)
     * /dependent => dependent.js (basedir is stripped and ignores parent)
     * other/dependent => other/dependent.js (ignores parent)
     * wrappy => node_modules/wrappy/wrappy.js (resolved using nodejs require algorithm)
     * If the resolved module id is not in the cache it is resolved using the NodeJs
     * resolve algorithm.
     * @param dependentModuleId The module path to resolve which is relative to
     * the {@link parentModuleId}, absolute or relative to the project root.
     * @param parentModuleId The module path relative to the project root to which the given
     * {@link dependentModuleId} is relative.
     * @returns Returns the module id for the dependent module which is now relative to the
     * project root or in case the resolved module id is not in the cache it resolves it to
     * an absolute module id using the NodeJs resolve algorithm. The output is in Unix (/)
     * path style to ensure module ids are consistent between Windows and Unix systems.
     */
    resolveDependentModuleId(dependentModuleId, parentModuleId) {
        let rootModuleId;
        // If relative to project root do not change.
        if (dependentModuleId.search(/^\.{0,2}(?:\/|\\)/) === -1) {
            rootModuleId = dependentModuleId;
            // If absolute remove baseDir.
        }
        else if (dependentModuleId.search(/^(?:\/|\\)/) !== -1) {
            rootModuleId = path.relative(this.baseDir, dependentModuleId);
            // If relative to parent make it relative to the project root.
        }
        else {
            const absoluteModuleId = path.resolve(this.baseDir, path.dirname(parentModuleId), dependentModuleId);
            rootModuleId = path.relative(this.baseDir, absoluteModuleId);
        }
        // Normalize to Unix path style so output is consistent between Windows and Unix systems.
        rootModuleId = rootModuleId.replace(/\\/g, "/");
        // If file is in the stream (cache) use it, otherwise resolve it using NodeJs resolve
        // algorithm.
        return new Promise((resolve, reject) => {
            if (rootModuleId.search(/\.js$/) === -1) {
                rootModuleId = rootModuleId + ".js";
            }
            if (this.cache[rootModuleId]) {
                resolve(rootModuleId);
            }
            else {
                // Input of nodeResolve cannot begin with 'node_modules' or end with '.js'.
                nodeResolve(rootModuleId.replace(/\.js$/, "").replace(/^.*node_modules(?:\/)/, ""), { basedir: this.baseDir }, (error, absoluteModuleId) => {
                    if (error || !absoluteModuleId) {
                        reject(error);
                    }
                    else {
                        // Always start with node_modules so it also works if this folder
                        // is a few levels above baseDir, i.e. a baseDir of '/path/src/' and
                        // the node_modules here '/path/node_modules'
                        // Also normalize to Unix path style so output is consistent between
                        // Windows and Unix systems.
                        resolve(absoluteModuleId.replace(/^.*(?=node_modules)/, "").replace(/\\/g, "/"));
                    }
                });
            }
        });
    }
}
exports.default = CreateBundle;
