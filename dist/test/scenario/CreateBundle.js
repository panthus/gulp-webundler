"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Bundler_1 = require("../../src/Bundler");
/**
 * A scenario to create a bundle given {@link vinyl} files and then assert the
 * result.
 */
class CreateBundle {
    /**
     * Create a {@link cache} given the specified {@link vinyl} files.
     * @param files The {@link vinyl} files to write to the stream.
     * @returns Returns this scenario to allow for fluent syntax.
     */
    static given(files, bundleSpecification) {
        const processedFiles = new Promise((resolve, reject) => {
            const stream = Bundler_1.default(bundleSpecification);
            files.forEach(file => { stream.write(file); });
            const filesInProgress = [];
            stream.on("data", (data) => { filesInProgress.push(data); });
            stream.on("end", () => { resolve(filesInProgress); });
            stream.on("error", (error) => { reject(error); });
            stream.end();
        });
        return new CreateBundle(processedFiles);
    }
    /**
     * Initializes a new instance of the {@link CreateBundle} class.
     * @param files The files to create this scenario for.
     */
    constructor(files) {
        this.files = files;
        this.tasks = [];
    }
    /**
     * Assert that all the specified bundles are returned.
     * @param modules An array containing the expected modules. If the bundle
     * property is undefined it is ignored.
     * @returns Returns this scenario to allow for fluent syntax.
     */
    expectAllSpecifiedBundles(modules) {
        const includedBundleNames = {};
        this.files
            .then(f => {
            f.forEach(x => { includedBundleNames[x.relative] = true; });
            modules.forEach(module => {
                if (module.bundle !== undefined) {
                    chai_1.expect(includedBundleNames[module.bundle]).to.be.true;
                }
            });
        });
        return this;
    }
    /**
     * Assert that the dependent module ids of the modules in the bundle are resolved to
     * relative paths without a file extension.
     * @param modules An array containing the expected modules.
     * @returns Returns this scenario to allow for fluent syntax.
     */
    expectRelativePathsWithoutExtension(modules) {
        this.files.then(f => {
            f.forEach(data => {
                this.tasks.push(this.read(data).then(outBundle => {
                    // Everything in the bundle should match.
                    modules.filter(file => data.relative === file.bundle).forEach(file => {
                        chai_1.expect(outBundle).to.match(new RegExp("\"" + this.escapeForRegex(file.id) +
                            "\":[function(require,module,exports){\n[\\w\\W]*},{" +
                            this.escapeForRegex(file.deps) + "}\]"));
                    });
                    // Everything not in the bundle should not match.
                    modules.filter(file => data.relative !== file.bundle).forEach(file => {
                        chai_1.expect(outBundle).to.not.match(new RegExp("\"" + this.escapeForRegex(file.id) +
                            "\":[function(require,module,exports){\n[\\w\\W]*},{" +
                            this.escapeForRegex(file.deps) + "}\]"));
                    });
                }));
            });
        });
        return this;
    }
    /**
     * Assert that the resulting bundle contains the correct source for each resolved path which should be in the
     * bundle.
     * @param modules An array containing the expected modules.
     * @returns Returns this scenario to allow for fluent syntax.
     */
    expectSourceForResolvedPath(modules) {
        this.files.then(f => {
            f.forEach(data => {
                this.tasks.push(this.read(data).then(outBundle => {
                    modules.filter(file => data.relative === file.bundle).forEach(file => {
                        chai_1.expect(outBundle)
                            .to.contain("\"" + file.id + "\":[function(require,module,exports){\n" + file.src);
                    });
                }));
            });
        });
        return this;
    }
    /**
     * Ends the scenario by ending the underlying stream.
     * @returns Returns the promise which indicates if the scenario is finished.
     */
    done() {
        return Promise.all(this.tasks.concat(this.files));
    }
    /**
     * Escape the given value so it can be used inside a regex.
     * @param value The value to escape.
     * @returns Returns the escaped value.
     */
    escapeForRegex(value) {
        return value.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
    }
    /**
     * Reads the vinyl file which can be a buffer or stream.
     * @returns Returns the contents of the file.
     */
    read(data) {
        return new Promise((resolve, reject) => {
            if (data.isStream()) {
                let contents = "";
                data.contents.on("data", chunk => { contents += chunk; });
                data.contents.on("end", () => { resolve(contents); });
                data.contents.on("error", error => { reject(error); });
            }
            else if (data.isBuffer()) {
                resolve(data.contents.toString());
            }
            else {
                resolve("");
            }
        });
    }
}
exports.default = CreateBundle;
