# Gulp-webundler

A utility which uses the internals from [Browserify](http://browserify.org/) to create web-bundles from an input stream of JavaScript files in [vinyl](https://github.com/gulpjs/vinyl) format. It tries to find the modules used for bundling inside the input stream. If a module cannot be found the NodeJs require algorithm is used to locate it.

## Usage
The following sample `gulpfile.js` should get you going.
```javascript
"use strict";
const gulp = require("gulp");
const ts = require("gulp-typescript");
const bundler = require("gulp-webundler").default;

const tsProject = ts.createProject({
	target: "ES5",
	module: "commonjs",
	noExternalResolve: true
});

gulp.task("build", () => {
    return gulp.src("*.ts")
	    .pipe(ts(tsProject))
	    .pipe(bundler([
            { name: "bundles/Common", include: ["Event", "Maybe"] },
            { name: "bundles/MainModel", include: ["TestModel"], exclude: ["bundles/Common"]}
 	    ]))
        .pipe(gulp.dest("dist"));
});
```

## API

The entry point of the utility consists of the `bundle` function in `Bundler.ts`.
```typescript
/**
 * Creates the stream which transforms the vinyl files into a buffer and
 * outputs bundles.
 * @param bundleSpecification The specifications of the bundles which should
 * be generated from the inputted vinyl files.
 * @returns Returns the stream.
 */
bundle(bundleSpecification: IBundleSpecification[]): stream.Transform;
```

It takes an array of `IBundleSpecification` objects.
```typescript
/**
 * Specifies which modules should make up the specified bundle.
 */
interface IBundleSpecification {
    /**
     * The name of the bundle without extension. The '.js' extension is
     * automatically added. If it is a path, then the specified folders will be
     * created in the output directory.
     */
    name: string;

    /**
     * The paths to the root modules to include in the bundle or module 
     * specification objects. Any dependent modules are automatically included
     * in the bundle. File extensions should be exluded.
     */
    include: (string | IModuleSpecification)[];

    /**
     * The paths to the root modules or bundle names to exclude from the bundle.
     * File extensions should be exluded.
     */
    exclude?: string[];
}

/**
 * Specifies how a module should be resolved.
 */
interface IModuleSpecification {
    /**
     * The path to the root module to include in the bundle. File extensions
     * should be excluded.
     */
    id: string;

    /**
     * The name the module should be exposed as in the bundle.
     */
    name: string;
}
```
