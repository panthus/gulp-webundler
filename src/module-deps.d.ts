declare namespace ModuleDeps {
    /**
     * A file cache map consisting of key value pairs. The key is the path to a
     * source file excluding its extension. The value consists of a buffer or
     * readable stream containing the source code of the file.
     */
    export interface IFileCache {
        [index: string]: Buffer | NodeJS.ReadableStream | null;
    }

    /**
     * The output of module-deps are modules with this signature.
     */
    export interface IModule {
        /**
         * The module path including filename and including extension.
         */
        id: string;
        /**
         * The module path including filename and excluding extension.
         */
        file: string;
        /**
         * The source code of the module.
         */
        source: string;
        /**
         * A map containing the dependencies of the module. The key is the
         * module id as specified in the file and value is the resolved module
         * id.
         */
        deps: { [index: string]: string };
    }

    /**
     * Options which can be passed to module-deps.
     */
    export interface IOptions {
        /**
         * An object mapping filenames to raw source to avoid reading from disk.
         */
        fileCache?: IFileCache;
        /**
         * A custom resolve function with the same signature as browser-resolve.
         */
        resolve?: (
            id: string,
            parent: {id: string},
            cb: (error: any, path: string | undefined) => void) => void;
        /**
         * The directory that is used to resolve module ids.
         */
        basedir?: string;
    }

    export interface IModuleDeps {
        (opts: IOptions): IModuleDeps;
        write(module: { id?: string, file: string }): boolean;
        on(event: "data", listener: (data: IModule) => void): IModuleDeps;
        on(event: "error", listener: (error: string) => void): IModuleDeps;
        on(event: "end" | string, listener: () => void): IModuleDeps;
        end(): void;
    }
}

declare module "module-deps" {
    const deps: ModuleDeps.IModuleDeps;
    export = deps;
}
