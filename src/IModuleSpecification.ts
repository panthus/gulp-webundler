/**
 * Specifies how a module should be resolved.
 */
interface IModuleSpecification {
    /**
     * The path to the root module to include in the bundle. File extensions
     * should be excluded.
     */
    id: string;

    /**
     * The name the module should be exposed as in the bundle.
     */
    name: string;
}

export default IModuleSpecification;
