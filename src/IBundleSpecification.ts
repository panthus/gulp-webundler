import IModuleSpecification from "./IModuleSpecification";

/**
 * Specifies which modules should make up the specified bundle.
 */
interface IBundleSpecification {
    /**
     * The name of the bundle without extension. The '.js' extension is
     * automatically added. If it is a path, then the specified folders will be
     * created in the output directory.
     */
    name: string;

    /**
     * The paths to the root modules to include in the bundle or module
     * specification objects. Any dependent modules are automatically included
     * in the bundle. File extensions should be exluded.
     */
    include: Array<(string | IModuleSpecification)>;

    /**
     * The paths to the root modules or bundle names to exclude from the bundle.
     * File extensions should be exluded.
     */
    exclude?: string[];
}

export default IBundleSpecification;
