"use strict";
import * as util from "gulp-util";
import * as ModuleDeps from "module-deps";
import * as stream from "stream";
import through = require("through2");
import * as File from "vinyl";
import CreateBundle from "./CreateBundle";
import IBundleSpecification from "./IBundleSpecification";

/**
 * A stream transforming the input consisting of vinyl files into an output
 * stream consisting of bundles.
 */
class Bundler {

    /**
     * Creates the stream which transforms the vinyl files into a buffer and
     * outputs bundles.
     * @param bundleSpecification The specifications of the bundles which should
     * be generated from the inputted vinyl files.
     * @returns Returns the stream.
     */
    public static bundle(
        bundleSpecification: IBundleSpecification[]): stream.Transform {
        const bundle = new Bundler(bundleSpecification);
        return bundle.createStream();
    }

    /**
     * A cache of the files from the input stream. It is a map with key
     * resolved module id and value module source code.
     */
    private cache: ModuleDeps.IFileCache;

    /**
     * The transform stream which transforms the vinyl files into a buffer and
     * outputs bundles.
     */
    private stream: stream.Transform;

    /**
     * The specifications of the bundles which should be generated from the
     * inputted vinyl files. Note that bundle names in the exclude property
     * have already been replaced by the root module ids of these bundles.
     */
    private bundleSpecification: IBundleSpecification[];

    /**
     * The base directory used by the Vinyl files. The modules ids in the outputted
     * bundles will be relative to this directory (only local modules).
     */
    private baseDir: string;

    /**
     * Initializes a new instance of the {@link Bundler} class.
     * @param bundleSpecification The specifications of the bundles which should
     * be generated from the inputted vinyl files.
     */
    constructor(bundleSpecification: IBundleSpecification[]) {
        // Create bundle name to included modules map.
        const bundleMap =
            bundleSpecification.reduce<{ [index: string]: string[] }>(
                (previous, current) => {
                    previous[current.name] = current.include
                        .map(x => x instanceof String || typeof x === "string" ? x : x.name) as string[];
                    return previous;
                },
                {});
        // Replace all bundle names in the exclude for the actual root module ids.
        this.bundleSpecification = bundleSpecification.map(x => {
            return {
                exclude: x.exclude
                    ? x.exclude.reduce<string[]>(
                        (previous, current) => bundleMap[current]
                            ? previous.concat(bundleMap[current])
                            : previous.concat(current),
                        [])
                    : x.exclude,
                include: x.include,
                name: x.name,
            };
        });
    }

    /**
     * Creates the stream which transforms the vinyl files into a cache and
     * outputs bundles.
     * @returns Returns the stream.
     */
    private createStream(): stream.Transform {
        this.cache = {};
        this.stream = through.obj(
            (data, enc, cb) => { this.transform(data, enc, cb); },
            cb => { this.flush(cb); });

        return this.stream;
    }

    /**
     * Create the cache from the input.
     */
    private transform(
        data: File, enc: string, cb: (error?: any, data?: any) => void): void {
        // Normalize to Unix path style so output is consistent between Windows and Unix systems.
        this.baseDir = data.base.replace(/\\/g, "/");
        this.cache[data.relative.replace(/\\/g, "/")] = data.contents;
        cb();
    }

    /**
     * Output the bundles when they are created.
     */
    private flush(cb: (error?: util.PluginError) => void): void {
        const createBundle = new CreateBundle(this.cache, this.baseDir);

        const pendingBundles = this.bundleSpecification.map(x => {
            return createBundle.create(x).then(bundle => {
                this.stream.push(bundle);
            });
        });

        Promise.all(pendingBundles)
            .then(() => { cb(); })
            .catch(error => {
                cb(new util.PluginError("gulp-webundler", error));
            });
    }
}

export default Bundler.bundle;
